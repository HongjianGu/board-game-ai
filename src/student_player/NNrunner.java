package student_player;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class NNrunner {
	
	static NN neuralNetwork = new NN();
	
    public void run(){		
    	try {
    		int[][] inputTrainingData = readInputs("data/train.txt");
    		int[] 		    inputTargets = readTargets("data/target.txt");
    		neuralNetwork = new NN(inputTrainingData, inputTargets);
//    		for(int i = 0; i<10; i++) {
//    			for(int j = 0; j < 36; j++)
//    				System.out.print(inputTrainingData[i][j]+"; ");
//    			System.out.println();
//    			System.out.println(inputTargets[i]);
//    		}
    		
    	} catch (IOException e) {
            e.printStackTrace();
        }      
    }
    
    public void learn() {
    	neuralNetwork.learn();
    }
    
    public double eval(int[] inputs) {
    	return neuralNetwork.eval(inputs);
    }
    
    public double learningEval(int[] inputs, double randomResult) {
    	return neuralNetwork.learningEval(inputs, randomResult);
    }
    
    public void record() {
    	neuralNetwork.recordPara();
    }
    
    public void loadConfiguration() {
    	try {
			neuralNetwork.loadConfig();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public static int[][] readInputs(String file) throws IOException{
        List<String> lines = Files.readAllLines(Paths.get(file), StandardCharsets.UTF_8);
        int[][]   inputs = new int[lines.size()][];

        for(int i = 0; i<lines.size(); i++){
        	inputs[i] = stringToInt(lines.get(i).split(";"));
        }
        return inputs;
    }

    public static int[] readTargets(String file) throws IOException{ 
        List<String> lines = Files.readAllLines(Paths.get(file), StandardCharsets.UTF_8);
        int[] 	   targets = new int[lines.size()];

        for(int i = 0; i<lines.size(); i++){
        	targets[i] = Integer.parseInt(lines.get(i));
        }
        return targets;
    }

    // convert from an array of strings to an array of double when reading files
    private static double[] stringToDouble(String[] strings){
    	double[] stringToDouble = new double[strings.length];
        for (int i = 0; i <strings.length; i++) {
        	stringToDouble[i] = Double.parseDouble(strings[i]);
        }
        return stringToDouble;
    }
    
    // convert from an array of strings to an array of int when reading files
    private static int[] stringToInt(String[] strings){
    	int[] stringToInt = new int[strings.length];
        for (int i = 0; i <strings.length; i++) {
        	stringToInt[i] = Integer.parseInt(strings[i]);
        }
        return stringToInt;
    }
}
