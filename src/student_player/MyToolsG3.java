package student_player;

import java.util.ArrayList;
import java.util.Collections;
import boardgame.Board;
import boardgame.Move;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoBoardState.Piece;
import pentago_swap.PentagoMove;

public class MyToolsG3 {

    public static Move intelligentMove(Piece p, PentagoBoardState pbs){
    	double max = -100;
    	Move bestMove = null;
    	for(PentagoMove m : pbs.getAllLegalMoves()) {
    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
    		child.processMove(m);
    		double val = minimaxEval(1, p, child);
    		if(val>max) {
    			max = val;
    			bestMove = m;
//    			System.out.println("Improving: " + max);
    		}
    	}
//    	System.out.println("Step value estimated: " + max);
    	return bestMove;
    }
    
    public static double minimaxEval(int depth, Piece p, PentagoBoardState pbs) {
    	return miniMax(depth, p, pbs, false, -100, 100);
    }
    
    // alpha-beta pruning algorithm seen in class
    public static double miniMax(int depth, Piece p, PentagoBoardState pbs, boolean maximize, double alpha, double beta) {
    	int id = p == Piece.WHITE ? 0 : 1;
    	if(depth == 0) return evalRandom(id, pbs, 16);
    	if(maximize) {
    		for(PentagoBoardState child: getSubStates(p, pbs)) {
        		alpha = Math.max(alpha, miniMax(depth-1, p, child, !maximize, alpha, beta));
        		if(alpha >= beta) break;
        	}
    		return alpha;
		}else {
			for(PentagoBoardState child: getSubStates(p, pbs)) {
				beta = Math.min(beta, miniMax(depth-1, p, child, !maximize, alpha, beta));
        		if(alpha >= beta) break;
        	}
    		return beta;
		}
    }
    
    public static ArrayList<PentagoBoardState> getSubStates(Piece p, PentagoBoardState pbs) {
    	int id = p == Piece.WHITE ? 0 : 1;
    	ArrayList<PentagoBoardState> children = new ArrayList<PentagoBoardState>();
    	if(pbs.getWinner() == Board.NOBODY) {
    		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
    		Collections.shuffle(moves);
    		double max = 0.4;
    		int count = 2;
    		for(PentagoMove m :moves) {
    			if(count <= 0) break;
        		// clone pbs and process the move, then add to children
        		PentagoBoardState child = (PentagoBoardState) pbs.clone();
        		child.processMove(m);
        		double eval = evalRandom(id, pbs, 10);
        		if(eval > max) {
        			children.add(child);
        			count--;
        		}
        		
        	}
    	}
    	
		return children;
    }
    
    private static double evalRandom(int id, PentagoBoardState pbs, int rep) {
    	double win = 0;
    	int i = 0;
    	while(i < rep) {
    		win += playRandom(id, pbs);
    		i++;
    	}
    	return win/rep;
    }
    
    public static int playRandom(int id, PentagoBoardState pbs) {
    	
    	if (pbs.getWinner() == id) return 1;
    	else if(pbs.getWinner() != Board.NOBODY) return 0;
    	pbs = (PentagoBoardState) pbs.clone();
    	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
        pbs.processMove(moves.get((int)(Math.random()*moves.size())));
		return playRandom(id,pbs);
    }
}