//package student_player;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Random;
//
//import boardgame.Board;
//import boardgame.Move;
//import pentago_swap.PentagoBoardState;
//import pentago_swap.PentagoBoardState.Piece;
//import pentago_swap.PentagoMove;
//
//public class MyTools {
//	
//	private static final double POS = Double.NEGATIVE_INFINITY;
//	private static final double NEG = Double.POSITIVE_INFINITY;
//	private static final double MIN = -100000000;
//	private static final double MAX = 100000000;
//	private static Move bestMove = null;
//	private static final int DEPTH = 3;
//	private static double score = MIN;
////	public static Move minimaxMove(Piece p, PentagoBoardState pbs){
////    	double max = MIN;
////    	Move bestMove = null;
////    	double[] alphaBeta = {MIN,MAX};
////    	for(PentagoMove m : pbs.getAllLegalMoves()) {
////    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
////    		child.processMove(m);
////    		double val = miniMax(3, p, child, false, alphaBeta);
////    		if(val>max) {
////    			max = val;
////    			bestMove = m;
//////    			System.out.println("Improving: " + max);
////    		}
////    	}
////    	System.out.println("Step value estimated: " + max);
////    	return bestMove;
////    }
////    
////    // alpha-beta pruning algorithm seen in class
////    public static double miniMax(int depth, Piece p, PentagoBoardState pbs, boolean maximize, double[] alphaBeta) {
////    	if(depth == 0 || pbs.gameOver()) return Heuristic.evalBoard(p, pbs);
////    	
////    	if(maximize) {
////    		for(PentagoBoardState child: getSubStates(pbs)) {
////    			alphaBeta[0] = Math.max(alphaBeta[0], miniMax(depth-1, p, child, !maximize, alphaBeta));
////        		if(alphaBeta[0] >= alphaBeta[1]) break;
////        	}
////    		return alphaBeta[0];
////		}else {
////			for(PentagoBoardState child: getSubStates(pbs)) {
////				alphaBeta[1] = Math.min(alphaBeta[1], miniMax(depth-1, p, child, !maximize, alphaBeta));
////        		if(alphaBeta[0] >= alphaBeta[1]) break;
////        	}
////    		return alphaBeta[1];
////		}
////    }
//    
//	public static Move minimaxMove(Piece p, PentagoBoardState pbs){
////    	double max = MIN;
//    	bestMove = pbs.getRandomMove();
////    	Move bestMove = null;
////    	double score = MIN-1;
////    	for(PentagoMove m : pbs.getAllLegalMoves()) {
////    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
////    		child.processMove(m);
//////    		double eval = Heuristic.evalBoard(p, child);
//////    		if(eval > score) {
//////    			score = eval;
////    			double val = miniMax(2, p, child, false, MIN, MAX);
////	    		if(val>max) {
////	    			max = val;
////	    			bestMove = m;
////	    			//System.out.println("Improving: " + max);
////	    		}
//////    		}
////    		
////    	}
////    	System.out.println("Step value estimated: " + max);
//    	pbs = (PentagoBoardState) pbs.clone();
//    	score = MIN;
//    	miniMax(DEPTH, p, pbs, true, MIN, MAX);
//    	return bestMove;
//    }
//    
//    // alpha-beta pruning algorithm seen in class
//    public static double miniMax(int depth, Piece p, PentagoBoardState pbs, boolean maximize, double alpha, double beta) {
//    	if(depth == 0 || pbs.gameOver()) return Heuristic.evalBoard(p, pbs);
//    	int id = p == Piece.WHITE ? 0 : 1;
//    	if(maximize) {
////    		Piece self = pbs.getTurnPlayer() == 0 ? Piece.WHITE : Piece.BLACK;
////        	System.out.println("MAX PLAYER");
//        	double max = MIN;
//        	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
//        	Collections.shuffle(moves);
////        	System.out.println("Initial ALPHA: "+alpha);
//        	if(moves.size()==0)System.out.println("No Moves!!!!");
////        	else System.out.println("Number of possible moves: "+moves.size());
//    		for(PentagoMove m : moves) {
//        		// clone pbs and process the move, then add to children
//        		PentagoBoardState child = (PentagoBoardState) pbs.clone();
//        		child.processMove(m);
////        		if(child.getWinner() == id) {
////        			alpha = POS;
////        			return POS;
////        		}else if(child.getWinner() == (1-id)) {
////        			return NEG;
////        		}
////        		double val = Heuristic.evalBoard(p, child);
////        		if(val>max) {
////        			max = val*0.8;
////        		}else if(val == max) ;
////        		else continue;
//        		alpha = Math.max(alpha, miniMax(depth-1, p, child, !maximize, alpha, beta));
////        		if(alpha == 30)System.out.println(m.getMoveCoord().getX() + ", " + m.getMoveCoord().getY() + " -- " +m.getASwap() + ", " + m.getBSwap());
////        		System.out.println("ALPHA: "+alpha);
////        		System.out.println("Score: "+score);
//        		if(depth == DEPTH && (alpha == POS || alpha>score)) {
//        			System.out.println(alpha);
////        			if(alpha == 30)System.out.println(m.getMoveCoord().getX() + ", " + m.getMoveCoord().getY() + " -- " +m.getASwap() + ", " + m.getBSwap());
//        			score = alpha;
//        			bestMove = m;
//        		}
//        		if(alpha >= beta) break;        		
//    		}
//    		return alpha;		
//        		
//		}else {
//			for(PentagoBoardState child: getSubStates(pbs)) {
//				beta = Math.min(beta, miniMax(depth-1, p, child, !maximize, alpha, beta));
//        		if(alpha >= beta) break;
//        	}
//    		return beta;
//		}
//    }
//	
//    /**
//     * get all children of the current pbs according to all legal moves
//     * @param pbs
//     * @return children of input pbs
//     */
//    public static ArrayList<PentagoBoardState> getSubStates(PentagoBoardState pbs) {
//    	
//    	Piece p = pbs.getTurnPlayer() == 0 ? Piece.WHITE : Piece.BLACK;
//    	ArrayList<PentagoBoardState> children = new ArrayList<PentagoBoardState>();
////    	Random r = new Random();
//    	double max = MIN;
////    	int count = 55;
//    	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
////    	Collections.shuffle(moves);
//    	if(!pbs.gameOver()) {
//    		for(PentagoMove m : moves) {
////    			if(r.nextDouble()<0.7) continue;
////    			if(count <= 0) break;
//        		// clone pbs and process the move, then add to children
//        		PentagoBoardState child = (PentagoBoardState) pbs.clone();
//        		child.processMove(m);
////        		double val = Heuristic.evalBoard(p, child);
////        		if(val>max) {
////        			max = val*0.5;
//        			children.add(child);
////        		}
//        		
////        		count--;
//        	}
//    	}
//    	Collections.shuffle(children);
//		return children;
//    }
//    
////	public static ArrayList<PentagoBoardState> getSubMoves(PentagoBoardState pbs) {
////		
////		ArrayList<PentagoBoardState> children = new ArrayList<PentagoBoardState>();
////		Random r = new Random();
////		int count = 5;
////		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
////		Collections.shuffle(moves);
////		if(pbs.getWinner() == Board.NOBODY) {
////			for(PentagoMove m : moves) {
////	//			if(r.nextDouble()<0.7) continue;
////				if(count <= 0) break;
////	    		// clone pbs and process the move, then add to children
////	    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
////	    		child.processMove(m);
////	    		children.add(child);
////	    		count--;
////	    	}
////		}
////		Collections.shuffle(children);
////		return children;
////	}
//	
////    public static Move chooseMove(Piece p, PentagoBoardState pbs){
////    	
////    	double max = MIN;
////    	double score = MIN;
////    	Move bestMove = pbs.getRandomMove();
////    	boolean maximize = false;
////    	if(p.equals(Piece.WHITE)) maximize = true;
////    	for(PentagoMove m : pbs.getAllLegalMoves()) {
////    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
////        	double value = Heuristic.evalMove(p, m, child);
////        	double val = MIN;
////    		if(value>score) {
////    			score = value;
////    			val = miniMax(4, p, m, maximize, MIN, MAX, child);
////    		}else continue;
////
//////    		System.out.println("Value: " + val);
////    		if(val>max) {
////    			max = val;
////    			bestMove = m;
//////    			System.out.println("Improving: " + max);
////    		}
////    	}    	
////    	System.out.println("Step value estimated: " + max);
////    	return bestMove;
////    }
////    
////    // alpha-beta pruning algorithm seen in class
////    public static double miniMax(int depth, Piece p, PentagoMove move, boolean maximize, double alpha, double beta, PentagoBoardState origin) {
//////    	if(depth == 0) return Heuristic.evalMove(p, move, origin);
//////    	origin = (PentagoBoardState) origin.clone();
//////    	origin.processMove(move);
////    	
////    	if(origin.gameOver()) return Heuristic.evalBoard(p, origin);
////    	if(depth == 0) return Heuristic.evalMove(p, move, origin);
////    	origin = (PentagoBoardState) origin.clone();
////    	origin.processMove(move);
////    	if(origin.gameOver()) return Heuristic.evalBoard(p, origin);
//////    	if(depth == 0) return Heuristic.evalBoard(p, origin);
////
////    	
////    	if(!maximize) {
//////    		ArrayList<PentagoMove> moves = origin.getAllLegalMoves();
////    		ArrayList<PentagoMove> moves = getBestMoves(origin);
////    		for(PentagoMove m: moves) {
////    			alpha = Math.max(alpha, miniMax(depth-1, p, m, !maximize, alpha, beta, origin));
////        		if(alpha >= beta) return alpha;
////    		}
////			return alpha;
////		}else {
//////    		ArrayList<PentagoMove> moves = origin.getAllLegalMoves();
////			ArrayList<PentagoMove> moves = getBestMoves(origin);
////			for(PentagoMove m: moves) {
////				beta = Math.min(beta, miniMax(depth-1, p, m, !maximize, alpha, beta, origin));
////	    		if(alpha >= beta) return beta;
////        	}
////    		return beta;		
////		}
////    }
////    
////    public static ArrayList<PentagoMove> getBestMoves(PentagoBoardState pbs) {
////    	
////    	if(pbs.gameOver()) System.out.println("Someone won!!!");
////    	
////    	ArrayList<PentagoMove> children = new ArrayList<PentagoMove>();
////		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
////    	Collections.shuffle(moves);
////
////    	double max = MIN;
////		for(PentagoMove m :moves) {
////    		// clone pbs, then add to children
////    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
////    		Piece p = child.getTurnPlayer() == 0 ? Piece.WHITE : Piece.BLACK;
////    		double val = Heuristic.evalMove(p, m, child);
////    		if(val>max) {
////    			max = val;
////    			children.add(m);
////    		}
////    	}
////		
////		if(children.size() == 0) System.out.println("No possible moves!!!");
////		
////    	Collections.shuffle(children);
////		return children;
////    }
//
////    public static ArrayList<PentagoMove> getMaxSubMoves(Piece p, PentagoBoardState pbs) {
////    	
////    	ArrayList<PentagoMove> children = new ArrayList<PentagoMove>();
////		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
////    	Collections.shuffle(moves);
////		double max = MIN;
////		for(PentagoMove m :moves) {
////    		// clone pbs, then add to children
////    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
//////    		Piece p = child.getTurnPlayer() == 0 ? Piece.WHITE : Piece.BLACK;
////    		double val = Heuristic.defenseEval(p, m, child);
////    		if(val>max) {
////    			max = val;
////    			children.add(m);
////    		}
////    	}
////    	Collections.shuffle(children);
////		return children;
////    }
////    
////    public static ArrayList<PentagoMove> getMinSubMoves(Piece p, PentagoBoardState pbs) {
////    	
////    	ArrayList<PentagoMove> children = new ArrayList<PentagoMove>();
////		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
////    	Collections.shuffle(moves);
////		double min = MAX;
////		for(PentagoMove m :moves) {
////    		// clone pbs, then add to children
////    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
//////    		Piece p = child.getTurnPlayer() == 1 ? Piece.WHITE : Piece.BLACK;
////    		double val = Heuristic.defenseEval(p, m, child);
////    		if(val<min) {
////    			min = val;
////    			children.add(m);
////    		}
////    	}
////    }
//}