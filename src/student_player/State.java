//package student_player;
//
//import pentago_swap.PentagoBoardState;
//import pentago_swap.PentagoBoardState.Piece;
//import pentago_swap.PentagoMove;
//
//public class State implements Comparable<State>{
//
//	public PentagoBoardState pbs;		// the corresponding pbs
//	public PentagoMove		 from;		// the move taken to get current pbs
//	public double			 value;		// the value of this state
//	//public Piece[][]		 board;		// the board configuration
//	//public ArrayList<State> children;	// the children of the state
//	
//	private int				depth = 2;	// minimax search depth
//	private int					id;		// student_player
//	//private Piece 				p;		// piece holding
//	
//	State(Piece p, PentagoBoardState pbs, PentagoMove from) {
//		this.pbs = pbs;
//		this.from = from;
//		//this.p = p;
//		this.id = p == Piece.WHITE ? 0 : 1;
//		this.value = MyTools.miniMax(depth, p, pbs, pbs.getTurnPlayer()==id, -100, 100);
//		//if(!pbs.gameOver()) children = MyTools.getChildState(p, pbs);
//		//if (pbs.getTurnPlayer()==id) System.out.println("MAIMIZE!");
//	}
//	
//	public void setDepth(int d) {
//		this.depth = d;
//	}
//	
////	public boolean equals(State s) {
////		return Arrays.equals(this.board, s.board);
////	}
//
//	@Override
//	public int compareTo(State s) {
//		return this.value > s.value ? 1 : this.value < s.value ? -1 : 0;
//	}
//
//}
