package student_player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import boardgame.Board;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoMove;
import pentago_swap.PentagoBoardState.Piece;

public class SimpleMinimax {

	public static double minimaxEval(int depth, Piece p, PentagoBoardState pbs) {
    	return miniMax(depth, p, pbs, false, Double.MIN_VALUE, Double.MAX_VALUE);
    }
    
    // alpha-beta pruning algorithm seen in class
    private static double miniMax(int depth, Piece p, PentagoBoardState pbs, boolean maximize, double alpha, double beta) {
    	if(depth == 0||pbs.getWinner() != Board.NOBODY) return SimpleEval.simpleEval(p, pbs);
//    	int id = p == Piece.WHITE ? 0 : 1;
    	if(maximize) {
//    		if(pbs.getWinner() == id) {alpha = 1;return 1;}
//    		else if(pbs.getWinner() == (1-id)) {alpha = 0; return 0;}
    		for(PentagoBoardState child: getSubStates(pbs)) {
        		alpha = Math.max(alpha, miniMax(depth-1, p, child, !maximize, alpha, beta));
        		if(alpha >= beta) break;
        	}
    		return alpha;
		}else {
//			if(pbs.getWinner() == id) {beta = 1; return 1;}
//    		else if(pbs.getWinner() == (1-id)) {beta = 0; return 0;}
			for(PentagoBoardState child: getSubStates(pbs)) {
				beta = Math.min(beta, miniMax(depth-1, p, child, !maximize, alpha, beta));
        		if(alpha >= beta) break;
        	}
    		return beta;
		}
    }
    
    public static ArrayList<PentagoBoardState> getSubStates(PentagoBoardState pbs) {
    	
    	ArrayList<PentagoBoardState> children = new ArrayList<PentagoBoardState>();
//    	Random r = new Random();
//    	int count = 65;
    	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
    	Collections.shuffle(moves);
    	if(pbs.getWinner() == Board.NOBODY) {
    		for(PentagoMove m : moves) {
//    			if(r.nextDouble()<0.7) continue;
//    			if(count <= 0) continue;
        		// clone pbs and process the move, then add to children
        		PentagoBoardState child = (PentagoBoardState) pbs.clone();
        		child.processMove(m);
        		children.add(child);
//        		count--;
        	}
    	}
    	Collections.shuffle(children);
		return children;
    }
}
