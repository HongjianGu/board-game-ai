package student_player;

import boardgame.Move;

import pentago_swap.PentagoPlayer;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoBoardState.Piece;

/** A player file submitted by a student. */
public class StudentPlayer extends PentagoPlayer {

    /**
     * You must modify this constructor to return your student number. This is
     * important, because this is what the code that runs the competition uses to
     * associate you with your agent. The constructor should do nothing else.
     */
    public StudentPlayer() {
        super("260772647");
    }

    /**
     * This is the primary method that you need to implement. The ``boardState``
     * object contains the current state of the game, which your agent must use to
     * make decisions.
     */
    //private Timer husTimer = new Timer();
//    public static NNrunner nn = new NNrunner();
//    boolean initialized = false;
    
    private static int id = -1;
    private Piece color;
    
    public Move chooseMove(PentagoBoardState boardState) {
        // You probably will make separate functions in MyTools.
        // For example, maybe you'll need to load some pre-processed best opening
        // strategies...
    	
    	id = boardState.getTurnPlayer();
    	color = id == 0 ? Piece.WHITE : Piece.BLACK;

//    	if(!initialized) {
//    		nn.run();
//    		nn.loadConfiguration();
//    		initialized = true;
//    	}
    	//System.out.println(id);
    	
    	//Move myMove = boardState.getRandomMove();    	


        // Return your move to be processed by the server.
        return MyTools.minimaxMove(color, boardState);
    }
}