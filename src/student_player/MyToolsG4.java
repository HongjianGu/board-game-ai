package student_player;

import java.util.ArrayList;
import boardgame.Board;
import boardgame.Move;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoMove;

public class MyToolsG4 {
	
	
    public static Move MCTSG4Move(int id, PentagoBoardState pbs) {
    	MCTSG4 mcts = new MCTSG4(id, pbs);
    	return mcts.findMove();
    }

    public static int playRandom(int id, PentagoBoardState pbs) {
    	
    	if (pbs.getWinner() == id) return 1;
    	else if(pbs.getWinner() != Board.NOBODY) return 0;
    	pbs = (PentagoBoardState) pbs.clone();
    	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
        pbs.processMove(moves.get((int)(Math.random()*moves.size())));
		return playRandom(id,pbs);
    }
    
    public static double evalRandom(int id, PentagoBoardState pbs, int rep) {
    	double win = 0;
    	int i = 0;
    	while(i < rep) {
    		win += playRandom(id, pbs);
    		i++;
    	}
    	return win/rep;
    }
}
