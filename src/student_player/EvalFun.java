package student_player;

import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoBoardState.Piece;
import pentago_swap.PentagoBoardState.Quadrant;
import pentago_swap.PentagoMove;

public class EvalFun {

	public static double evalBoard(Piece p, PentagoBoardState pbs) {
		int id = p == Piece.WHITE ? 0 : 1;
		if(pbs.getWinner() == id) {
			return 100000000;
		}else if(pbs.getWinner() == (1-id)) {
			return -100000000;
		}
		Piece oppo = p == Piece.WHITE ? Piece.BLACK : Piece.WHITE;
		return evaluate(parser(p, pbs)) - evaluate(parser(oppo, pbs));
	}
	
	public static double evalMove(PentagoMove move, PentagoBoardState pbs) {
		double score = 0;
		Piece p = pbs.getTurnPlayer() == 0 ? Piece.WHITE : Piece.BLACK;
		Piece poppo = pbs.getTurnPlayer() == 0 ? Piece.BLACK : Piece.WHITE;
		
		int[][] board = parser(p, pbs);
		int[][] boardoppo = parser(poppo, pbs);
		
		return score;
	}
	
	public static double evaluate(int[][] board) {

		double vself = 0;
		double voppo = 0;
		int vQuaCount = 0;
		
		double hself = 0;
		double hoppo = 0;
		int hQuaCount = 0;
		
		double d1self = 0;
		double d1oppo = 0;
		
		double d2self = 0;
		double d2oppo = 0;
		int d2QuaCount = 0;
		
		double vScore = 0;
		double hScore = 0;
		double d1Score = 0;
		double d2Score = 0;
		
		double coef = 1;
		double dcoef = 1;
		
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board.length; j++) {
				if((i == 1 || i == 4) && (j == 1 || j == 4)) dcoef = 1.2;
				else dcoef = 1;
				if(j == 1 || j == 4) coef = 1.2;
				else coef = 1;
				
				if(board[i][j] == -1) hoppo += 1 * coef;
				else if(board[i][j] == 1) {hself += 1 * coef; hQuaCount++;}
				
				if(board[j][i] == -1) voppo += 1 * coef;
				else if(board[j][i] == 1) {vself += 1 * coef; vQuaCount++;}
				
				if(i == 0) {
					if(j < 5 && board[j][j+1] == -1) d1oppo += 1;
					else if (j < 5 && board[j][j+1] == 1) d1self += 1;
					
					if(board[j][j] == -1) d2oppo += 1 * dcoef;
					else if(board[j][j] == 1) {d2self += 1 * dcoef; d2QuaCount++;}
				}else if(i == 1) {
					if(j < 5 && board[j+1][j] == -1) d1oppo += 1;
					else if (j < 5 && board[j+1][j] == 1) d1self += 1;
					
					if(board[j][5-j] == -1) d2oppo += 1 * dcoef;
					else if(board[j][5-j] == 1) {d2self += 1 * dcoef; d2QuaCount++;}
				}else if(i == 2 && j < 5) {
					if(board[4-j][j] == -1) d1oppo++;
					else if (board[4-j][j] == 1) d1self += 1;
				}else if(i == 3 && j > 0) {
					if(board[j][6-j] == -1) d1oppo++;
					else if (board[j][6-j] == 1) d1self += 1;
				}
				
				if(j == 2 || j == 5) {
					if(hQuaCount == 3) {
						hself+=0.3;
						if(board[i][(j-1+3)%6] == 1 && board[i][(j+3)%6] == 0 && board[i][(j-2+3)%6] == 0) hScore += 500;
						else if(board[i][(j-1+3)%6] == -1) hoppo += 2;
						else {
							double empty = 0;
							if(board[i][(j-1+3)%6] == 0) empty++;
							if(board[(i+3)%6][(j-1+3)%6] == 0) empty++;
							if(board[(i+3)%6][j-1] == 0) empty++;
							hself += empty/4;
						}
					}
					
					if(vQuaCount == 3) {
						vself+=0.3;
						if(board[(j-1+3)%6][i] == 1 && board[(j+3)%6][i] == 0 && board[(j-2+3)%6][i] == 0) vScore += 500;
						else if(board[(j-1+3)%6][i] == -1) voppo += 2;
						else {
							double empty = 0;
							if(board[j-1][(i+3)%6] == 0) empty++;
							if(board[(j-1+3)%6][i] == 0) empty++;
							if(board[(j-1+3)%6][(i+3)%6] == 0) empty++;
							vself += empty/4;
						}
					}
					
					if(d2QuaCount == 3) {
						d2self+=0.3;
						if(board[(j-1+3)%6][(j-1+3)%6] == 1 && board[(j+3)%6][(j+3)%6] == 0 && board[(j-2+3)%6][(j-2+3)%6] == 0) d2Score += 500;
						else if(board[(j-1+3)%6][(j-1+3)%6] == -1) d2oppo += 2;
						else {
							double empty = 0;
							if(board[(j-1+3)%6][(j-1+3)%6] == 0) empty++;
							if(board[(j-1+3)%6][j-1] == 0) empty++;
							if(board[j-1][(j-1+3)%6] == 0) empty++;
							d2self += empty/4;
						}
					}
					
					hQuaCount = 0;
					vQuaCount = 0;
					d2QuaCount = 0;
				}
			}
			vScore += helper(vself, voppo);
			hScore += helper(hself, hoppo);
			if(i == 1 || i == 0) {
				d2Score += helper(d2self, d2oppo);
//				d1Score = Math.max(d1Score, helper(d1self, d1oppo+0.1));
			}
			if(i != 4 && i != 5) {
				d1Score += helper(d1self+0.2, d1oppo+0.2);
			}
			vself = 0;
			voppo = 0;
			hself = 0;
			hoppo = 0;
			d1self = 0;
			d1oppo = 0;
			d2self = 0;
			d2oppo = 0;
			vQuaCount = 0;
			hQuaCount = 0;
			d2QuaCount = 0;
		}
		
//		double promising1 = Math.max(vScore, hScore);
//		double promising2 = Math.max(d2Score, 0.8*d1Score);
//		return Math.max(promising2, promising1);
		return vScore+hScore+d1Score+d2Score;
	}
	
	private static double helper(double self, double oppo) {
		if(oppo > 1) return 0;
		if(self >= 5) return 1000000;
		else if(oppo > 0.5) {
			return Math.pow(10, self);
		}else {
			return Math.pow(10, self);
		}
	}
	
	private static int[][] swapQuadrant(int[][] board, Quadrant a, Quadrant b){
		int temp;
		if(a == Quadrant.TL && b == Quadrant.TR) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j];
					board[i][j] = board[i][j+3];
					board[i][j+3] = temp;
				}
			}
		}else if(a == Quadrant.TL && b == Quadrant.BL) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j];
					board[i][j] = board[i+3][j];
					board[i+3][j] = temp;
				}
			}
		}else if(a == Quadrant.TL && b == Quadrant.BR) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j];
					board[i][j] = board[i+3][j+3];
					board[i+3][j+3] = temp;
				}
			}
		}else if(a == Quadrant.TR && b == Quadrant.BL) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j+3];
					board[i][j+3] = board[i+3][j];
					board[i+3][j] = temp;
				}
			}
		}else if(a == Quadrant.TR && b == Quadrant.BR) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j+3];
					board[i][j+3] = board[i+3][j+3];
					board[i+3][j+3] = temp;
				}
			}
		}else if(a == Quadrant.BL && b == Quadrant.BR) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i+3][j];
					board[i+3][j] = board[i+3][j+3];
					board[i+3][j+3] = temp;
				}
			}
		}else return swapQuadrant(board, b, a);
		return board;
	}
	
	private static int[][] parser(Piece p, PentagoBoardState pbs) {
		int[][] intBoard = new int[pbs.BOARD_SIZE][pbs.BOARD_SIZE];
    	for(int i = 0; i < intBoard.length; i++) {
			for(int j = 0; j < intBoard[0].length; j++) {
				Piece curP = pbs.getPieceAt(i,j);
				if(curP == p) intBoard[i][j] = 1;
				else if(curP == Piece.EMPTY) intBoard[i][j] = 0;
				else intBoard[i][j] = -1;
			}
		}
    	return intBoard;
	}
}
