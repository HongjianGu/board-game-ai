package student_player;

import java.util.Arrays;

import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoBoardState.Piece;
import pentago_swap.PentagoBoardState.Quadrant;
import pentago_swap.PentagoCoord;
import pentago_swap.PentagoMove;

public class Heuristic {
	
	private static final double MIN = Double.NEGATIVE_INFINITY;
	private static final double MAX = Double.POSITIVE_INFINITY;
	
	public static double evalBoard(Piece p,  PentagoBoardState pbs) {
		int id = p.equals(Piece.WHITE) ? 0 : 1;
		int opp = 1-id;
		if(pbs.getWinner() == id) {
			return MAX;
		}else if(pbs.getWinner() == opp) {
			return MIN;
		}else {
			Piece oppo = p.equals(Piece.WHITE) ? Piece.BLACK : Piece.WHITE;
			return evaluate(parser(p, pbs)) - evaluate(parser(oppo, pbs));
		}
//		return evaluate(parser(p, pbs));
	}
	
//	public static double evalMove(Piece p, PentagoMove move, PentagoBoardState origin) {
//		int id = p.equals(Piece.WHITE) ? 0: 1;
//		
////		if(origin.getWinner() == id) return MAX;
////		else if(origin.getWinner() == (1 - id)) return MIN;
//		
//		PentagoBoardState checkWin = (PentagoBoardState) origin.clone();
//		checkWin.processMove(move);
//		
//		if(checkWin.getWinner() == id) return MAX;
//		else if(checkWin.getWinner() == (1 - id)) return MIN;
//		
//		Piece opponent = p.equals(Piece.BLACK) ? Piece.WHITE : Piece.BLACK;
//		PentagoCoord coord = move.getMoveCoord();
//		
//		int[][] board = parser(p, origin);
//		double selfOrigin = evaluate(board);
//		
//		board[coord.getX()][coord.getY()] = 1;
//		double selfCur = evaluate(swapQuadrant(board, move.getASwap(), move.getBSwap()));
//		
//		int[][] oppoBoard = parser(opponent, origin);
////		if(evaluate(swapQuadrant(oppoBoard, move.getASwap(), move.getBSwap())) > 9000) return -1000000;
//		double oppoOrigin = evaluate(oppoBoard);
//		
//		oppoBoard[coord.getX()][coord.getY()] = 1;
//		double oppoCur = evaluate(swapQuadrant(oppoBoard, move.getASwap(), move.getBSwap()));
//		
////		if(oppoCur > 60) return oppoCur;
////		System.out.println((selfCur - selfOrigin) + "\t\t" + 0.5*(oppoCur - oppoOrigin));
//
////		if(selfCur > 90) return selfCur;
////		return Math.max((selfCur - selfOrigin), 0.6* (oppoCur - oppoOrigin));
////		return (selfCur*2 - selfOrigin) + 0.6*(oppoCur - oppoOrigin) - oppoCur;
//		return selfCur - oppoCur;
//	}
	
	private static int[][] swapQuadrant(int[][] board, Quadrant a, Quadrant b){
		int temp;
		if(a.equals(Quadrant.TL) && b.equals(Quadrant.TR)) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j];
					board[i][j] = board[i][j+3];
					board[i][j+3] = temp;
				}
			}
		}else if(a.equals(Quadrant.TL) && b.equals(Quadrant.BL)) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j];
					board[i][j] = board[i+3][j];
					board[i+3][j] = temp;
				}
			}
		}else if(a.equals(Quadrant.TL) && b.equals(Quadrant.BR)) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j];
					board[i][j] = board[i+3][j+3];
					board[i+3][j+3] = temp;
				}
			}
		}else if(a.equals(Quadrant.TR) && b.equals(Quadrant.BL)) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j+3];
					board[i][j+3] = board[i+3][j];
					board[i+3][j] = temp;
				}
			}
		}else if(a.equals(Quadrant.TR) && b.equals(Quadrant.BR)) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i][j+3];
					board[i][j+3] = board[i+3][j+3];
					board[i+3][j+3] = temp;
				}
			}
		}else if(a.equals(Quadrant.BL) && b.equals(Quadrant.BR)) {
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					temp = board[i+3][j];
					board[i+3][j] = board[i+3][j+3];
					board[i+3][j+3] = temp;
				}
			}
		}else return swapQuadrant(board, b, a);
		return board;
	}
	
	public static int[][] parser(Piece p, PentagoBoardState pbs) {
		int[][] intBoard = new int[PentagoBoardState.BOARD_SIZE][PentagoBoardState.BOARD_SIZE];
    	for(int i = 0; i < intBoard.length; i++) {
			for(int j = 0; j < intBoard[0].length; j++) {
				Piece curP = pbs.getPieceAt(i,j);
				if(curP.equals(p)) intBoard[i][j] = 1;
				else if(curP.equals(Piece.EMPTY)) intBoard[i][j] = 0;
				else intBoard[i][j] = -1;
			}
		}
    	return intBoard;
	}
	
	public static double evaluate(int[][] board) {

//		for(int[] i : board) {
//			System.out.println(Arrays.toString(i));
//		}
		double vself = 0;
		double voppo = 0;
		double vBonus = 0;
		int vQuaCount = 0;
		
		double hself = 0;
		double hoppo = 0;
		double hBonus = 0;
		int hQuaCount = 0;
		
		double d1self = 0;
		double d1oppo = 0;
		
		double d2self = 0;
		double d2oppo = 0;
		double d2Bonus = 0;
		int d2QuaCount = 0;
		
		double vScore = 0;
		double hScore = 0;
		double d1Score = 0;
		double d2Score = 0;
		
		double[] vScores = new double[6];
		double[] hScores = new double[6];
		double[] d1Scores = new double[4];
		double[] d2Scores = new double[2];
		
		double coef = 1;
		double dcoef = 1;
		
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board.length; j++) {
				if((i == 1 || i == 4) && (j == 1 || j == 4)) dcoef = 1.1;
				else dcoef = 1;
				if(j == 1 || j == 4) coef = 1.1;
				else coef = 1;
				
				if(board[i][j] == -1) hoppo += 1 * coef;
				else if(board[i][j] == 1) {hself += 1 * coef; hQuaCount++;}
				
				if(board[j][i] == -1) voppo += 1 * coef;
				else if(board[j][i] == 1) {vself += 1 * coef; vQuaCount++;}
				
				if(i == 0) {
					if(j < 5 && board[j][j+1] == -1) d1oppo += 1;
					else if (j < 5 && board[j][j+1] == 1) d1self += 1;
					
					if(board[j][j] == -1) d2oppo += 1 * dcoef;
					else if(board[j][j] == 1) {
						d2self += 1 * dcoef;
						d2QuaCount++;
//						if(j == 0) System.out.println("--------d2self++");
					}
				}else if(i == 1) {
					if(j < 5 && board[j+1][j] == -1) d1oppo += 1;
					else if (j < 5 && board[j+1][j] == 1) d1self += 1;
					
					if(board[5-j][j] == -1) d2oppo += 1 * dcoef;
					else if(board[5-j][j] == 1) {d2self += 1 * dcoef; d2QuaCount++;}
				}else if(i == 2 && j < 5) {
					if(board[4-j][j] == -1) d1oppo++;
					else if (board[4-j][j] == 1) d1self += 1;
				}else if(i == 3 && j > 0) {
					if(board[j][6-j] == -1) d1oppo++;
					else if (board[j][6-j] == 1) d1self += 1;
				}
				
//				if(j == 2 || j == 5) {
//					if(hQuaCount == 3) {
//						hBonus+=100;
////						if(board[i][(j-1+3)%6] == 1 && board[i][(j+3)%6] == 0 && board[i][(j-2+3)%6] == 0) hBonus += 5000;
//////						else if(board[i][(j-1+3)%6] == -1) hoppo += 0.2;
////						else {
////							double empty = 0;
////							if(board[i][(j-1+3)%6] == 0) empty++;
////							else if (board[i][(j-1+3)%6] == 1) empty+=2;
////							if(board[(i+3)%6][(j-1+3)%6] == 0) empty++;
////							else if (board[(i+3)%6][(j-1+3)%6] == 1) empty+=2;
////							if(board[(i+3)%6][j-1] == 0) empty++;
////							else if (board[(i+3)%6][j-1] == 1) empty+=2;
////							hBonus += empty*1000;
////						}
//					}
//					
//					hQuaCount = 0;
//					
//					if(vQuaCount == 3) {
//						vBonus+=100;
////						if(board[(j-1+3)%6][i] == 1 && board[(j+3)%6][i] == 0 && board[(j-2+3)%6][i] == 0) vBonus += 5000;
//////						else if(board[(j-1+3)%6][i] == -1) voppo += 0.2;
////						else {
////							double empty = 0;
////							if(board[j-1][(i+3)%6] == 0) empty++;
////							else if (board[j-1][(i+3)%6]== 1) empty+=2;
////							if(board[(j-1+3)%6][i] == 0) empty++;
////							else if (board[(j-1+3)%6][i] == 1) empty+=2;
////							if(board[(j-1+3)%6][(i+3)%6] == 0) empty++;
////							else if (board[(j-1+3)%6][(i+3)%6] == 1) empty+=2;
////							vBonus += empty*1000;
////						}
//					}
//					
//					vQuaCount = 0;
//					
//					if(d2QuaCount == 3) {
//						d2Bonus+=100;
////						if(board[(j-1+3)%6][(j-1+3)%6] == 1 && board[(j+3)%6][(j+3)%6] == 0 && board[(j-2+3)%6][(j-2+3)%6] == 0) d2Bonus += 5000;
//////						else if(board[(j-1+3)%6][(j-1+3)%6] == -1) d2oppo += 0.2;
////						else {
////							double empty = 0;
////							if(board[(j-1+3)%6][(j-1+3)%6] == 0) empty++;
////							else if (board[(j-1+3)%6][(j-1+3)%6] == 1) empty+=2;
////							if(board[(j-1+3)%6][j-1] == 0) empty++;
////							else if (board[(j-1+3)%6][j-1] == 1) empty+=2;
////							if(board[j-1][(j-1+3)%6] == 0) empty++;
////							else if (board[j-1][(j-1+3)%6] == 1) empty+=2;
////							d2Bonus += empty*1000;
////						}
//					}
//					
//					d2QuaCount = 0;
//				}
			}
//			vScore = helper(vself, voppo);
//			hScore = helper(hself, hoppo);
//			vScores[i] = vScore;
//			hScores[i] = hScore;
			vScore = Math.max(vScore, helper(vself, voppo));
			hScore = Math.max(hScore, helper(hself, hoppo));
//			if(vScore<=0||hScore<=0)System.out.println(vScore+"\t"+hScore);
			if(i == 1 || i == 0) {
//				d2Score = helper(d2self, d2oppo);
//				d2Scores[i] = d2Score;
				d2Score = Math.max(d2Score, helper(d2self, d2oppo));
//				if(d2Score>0)System.out.println("d2Score: " + d2Score);
//				d1Score = Math.max(d1Score, helper(d1self, d1oppo+0.1));
			}
			if(i != 4 && i != 5) {
//				d1Score = helper(d1self, d1oppo+0.1);
//				d1Scores[i] = d1Score;
				d1Score = Math.max(d1Score, helper(d1self, d1oppo+0.1));
			}
			vself = 0;
			voppo = 0;
			hself = 0;
			hoppo = 0;
			d1self = 0;
			d1oppo = 0;
			d2self = 0;
			d2oppo = 0;
			vQuaCount = 0;
			hQuaCount = 0;
			d2QuaCount = 0;
		}
		
//		double promising1 = Math.max(vScore, hScore);
//		double promising2 = Math.max(d2Score, 0.8*d1Score);
//		return Math.max(promising2, promising1);
//		if(true)System.out.println((int)vScore+"\t"+(int)hScore+"\t"+(int)d1Score+"\t"+(int)d2Score);
//		for(int k = 0; k < 3; k++) {
//			double temp = hScores[k];
//			hScores[k] += hScores[k+3] * 0.1;
//			hScores[k+3] += temp * 0.1;
//			
//			temp = vScores[k];
//			vScores[k] += vScores[k+3] * 0.1;
//			vScores[k+3] += temp * 0.1;
//		}
//		
//		vScore = 0;
//		hScore = 0;	
//		d1Score = 0;
//		d2Score = 0;
//
//		for(int k = 0; k < 6; k++) {
//			hScore += hScores[k];
//			vScore += vScores[k];
//			if(k<2) {
//				d2Score += d2Scores[k];
//			}
//			if(k<4) {
//				d1Score += d1Scores[k];
//			}
//		}
		
		return (vScore+hScore+d1Score+d2Score);
	}
	
	private static double helper(double self, double oppo) {
//		if(oppo >= 5) return -1000000;
//		else if(oppo >= 4 && self <= 1) return -1000000;
		
		if(oppo > 1) return 0;
		if(self == 0) return 0;
		if(self >= 5) return 1000000;
		if(oppo > 0.5) {
			return Math.pow(4, self);
		}else {
			return Math.pow(5, self);
		}
	}
}
