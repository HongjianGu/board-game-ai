package student_player;

import java.util.ArrayList;
import java.util.Collections;

import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoMove;

public class NodeG4 implements Comparable<NodeG4>{

	public PentagoBoardState pbs;
	public NodeG4 parent;
	public ArrayList<NodeG4> children;
	//public double UCTvalue;
	public PentagoMove from;
	public int numVisit;
	public int numWin;
	//public double score;
	public int id;
	
	public NodeG4(int id, PentagoBoardState pbs) {
		this.id = id;
		this.pbs = pbs;
		this.parent = null;
		this.children = new ArrayList<NodeG4>();
		this.from = null;
		this.numVisit = 0;
		this.numWin = 0;
		//this.UCTvalue = 0;
		//this.score = 0;
//		backPropogation(this,MyTools.playRandom(id, this.pbs));
		//this.UCTvalue = UCTvalue();
	}
	
	public NodeG4(NodeG4 node) {
		this.id = node.id;
		this.pbs = (PentagoBoardState) node.pbs.clone();
		this.parent = new NodeG4(node.parent);
		this.children = new ArrayList<NodeG4>();
		for(NodeG4 n: node.children) {
			children.add(new NodeG4(n));
		}
		//this.value = node.value;
		this.from = node.from;
		this.numVisit = node.numVisit;
		this.numWin = node.numWin;
		//this.score = node.score;
		//if(this.parent != null) this.UCTvalue = node.UCTvalue();
	}
	
//	public Node(Node parent, PentagoMove m, double initScore) {
//		this.id = parent.id;
//		this.pbs = (PentagoBoardState) parent.pbs.clone();
//		this.pbs.processMove(m);
//		this.parent = parent;
//		this.children = new ArrayList<Node>();
//		this.from = m;
//		this.numVisit = 0;
//		this.numWin = 0;
//		backPropogation(this,MyTools.playRandom(id, pbs));
//		this.UCTvalue = UCTvalue();
//		//this.score = initScore;
//	}
	
	public NodeG4(NodeG4 parent, PentagoMove m) {
		this.id = parent.id;
		this.pbs = (PentagoBoardState) parent.pbs.clone();
		this.pbs.processMove(m);
		this.parent = parent;
		this.children = new ArrayList<NodeG4>();
		this.from = m;
		this.numVisit = 0;
		this.numWin = 0;
//		backPropogation(this,MyTools.playRandom(id, this.pbs));
		//this.UCTvalue = UCTvalue();
		//this.score = 0;
	}
	
	public void populate() {
		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
		Collections.shuffle(moves);
//		int count =  15;
		for(PentagoMove m: moves) {
//			if(count <= 0) break;
			children.add(new NodeG4(this, m));
//			count--;
		}
	}
	
	public void selectPop() {
		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
		Collections.shuffle(moves);
		int count =  3;
		double max = 0.5;
		for(PentagoMove m: moves) {
			if(count <= 0) break;
			PentagoBoardState promisingNode = (PentagoBoardState) pbs.clone();
			promisingNode.processMove(m);
			double val = MyToolsG4.evalRandom(id, promisingNode, 30);
			if(val > max) {
				children.add(new NodeG4(this, m));
				max = val;
//				System.out.println("Selected child value: " + max);
				count--;
			}
//			System.out.println("Best selected child value: " + max);
		}
	}
	
//	public void minimaxPop(double rate) {
//		Piece p = id == 0 ? Piece.WHITE : Piece.BLACK;
//		double max = -100;
//		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
//		Collections.shuffle(moves);
//		int count = (int)(moves.size() * rate);
//    	for(PentagoMove m : moves) {
//    		if(count <= 0) break;
//    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
//    		child.processMove(m);
//    		double val = MyTools.minimaxEval(2, p, child);
//    		if(val>max) {
//    			max = val;
//    			children.add(new Node(this, m, max));
//    			System.out.println("Improving: " + max);
//    			count--;
//    		}
//    		//System.out.println("children selected: " + children.size() + " from: " + moves.size());
//    	}
//	}
	
	public double UCTvalue() {
        if (numVisit == 0) return Integer.MAX_VALUE;
        if(this.parent == null) return (double) numWin / (double) numVisit;
        return ((double) numWin / (double) numVisit) + Math.sqrt(Math.log(parent.numVisit) * 2 / (double) numVisit);
	}
	
	public NodeG4 bestUCTChild() {
		if(children.size() == 0) selectPop();
//		if(children.size() == 0) populate();
        return Collections.max(children);
    }
	
//	public Node bestMinimaxChild() {
//		if(children.size() == 0) minimaxPop(0.5);
//        return Collections.max(children);
//    }
	
	public NodeG4 randomChild() {
		if(children.size() == 0) {
//			populate();
			selectPop();
			if(children.size() == 0)populate();
			return children.get((int)(Math.random()*children.size()));
		}else {
			NodeG4 child = children.get((int)(Math.random()*children.size()));
//			backPropogation(child,MyToolsG4.playRandom(id, child.pbs));
			return child;
		}
//		if(children.size() != 0) System.out.println("WARNING: children is not empty!");
//		PentagoMove randomMove = (PentagoMove) pbs.getRandomMove();
//		Node randomChild = new Node(this, randomMove);
//		children.add(randomChild);
//		return randomChild;
	}

	public static void backPropogation(NodeG4 explored, int result) {
		NodeG4 tempNode = explored;
	    while (tempNode != null) {
	        tempNode.numVisit++;
	        tempNode.numWin += result;
	        //System.out.println(tempNode.numVisit + "\t" + tempNode.numWin);
	        tempNode = tempNode.parent;
//	        if(tempNode!=null && tempNode.parent == null) System.out.println("******Root*****:" + tempNode.numVisit + "\t" + tempNode.numWin + "\t" + tempNode.UCTvalue());
	    }
	}
	
	@Override
	public int compareTo(NodeG4 n) {
		return this.UCTvalue() > n.UCTvalue() ? 1 : this.UCTvalue() < n.UCTvalue() ? -1 : 0;
	}

}
