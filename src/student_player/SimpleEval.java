package student_player;

import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoBoardState.Piece;

public class SimpleEval {

	public static double simpleEval(Piece p, PentagoBoardState pbs) {
		int id = p == Piece.WHITE ? 0: 1;
		
		if(pbs.getWinner() == id) return 10000;
		else if(pbs.getWinner() == (1 - id)) return 0;
		
		Piece opponent = p == Piece.BLACK ? Piece.WHITE : Piece.BLACK;
		
		int[][] board = parser(p, pbs);		
		double self = evaluate(board);
		
		int[][] oppoBoard = parser(opponent, pbs);		
		double oppo = evaluate(oppoBoard);
		
//		if(oppoCur > 60) return oppoCur;
//		System.out.println((selfCur - selfOrigin) + "\t\t" + 0.5*(oppoCur - oppoOrigin));

//		if(selfCur > 90) return selfCur;
		return self - 1.5*oppo;
//		return (selfCur - selfOrigin) + 0.6*(oppoCur - oppoOrigin);
	}
	
	public static double evaluate(int[][] board) {

		double vself = 0;
		double voppo = 0;
		double hself = 0;
		double hoppo = 0;
		double d1self = 0;
		double d1oppo = 0;
		
		double d2self = 0;
		double d2oppo = 0;
		double vScore = 0;
		double hScore = 0;
		double d1Score = 0;
		double d2Score = 0;
		
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board.length; j++) {
				
				if(board[i][j] == -1) hoppo += 1;
				else if(board[i][j] == 1) {hself += 1;}
				
				if(board[j][i] == -1) voppo += 1;
				else if(board[j][i] == 1) {vself += 1;}
				
				if(i == 0) {
					if(j < 5 && board[j][j+1] == -1) d1oppo += 1;
					else if (j < 5 && board[j][j+1] == 1) d1self += 1;
					
					if(board[j][j] == -1) d2oppo += 1;
					else if(board[j][j] == 1) {d2self += 1;}
				}else if(i == 1) {
					if(j < 5 && board[j+1][j] == -1) d1oppo += 1;
					else if (j < 5 && board[j+1][j] == 1) d1self += 1;
					
					if(board[j][5-j] == -1) d2oppo += 1;
					else if(board[j][5-j] == 1) {d2self += 1;}
				}else if(i == 2 && j < 5) {
					if(board[4-j][j] == -1) d1oppo++;
					else if (board[4-j][j] == 1) d1self += 1;
				}else if(i == 3 && j > 0) {
					if(board[j][6-j] == -1) d1oppo++;
					else if (board[j][6-j] == 1) d1self += 1;
				}
			}
//			vScore = Math.max(vScore, helper(vself, voppo));
//			hScore = Math.max(hScore, helper(hself, hoppo));
			vScore += helper(vself, voppo);
			hScore += helper(hself, hoppo);
			if(i == 1 || i == 0) {
//				d2Score = Math.max(d2Score, helper(d2self, d2oppo));
				d2Score += helper(d2self, d2oppo);
			}
			if(i != 4 && i != 5) {
//				d1Score = Math.max(d1Score, helper(d1self, d1oppo+0.1));
				d1Score += helper(d1self, d1oppo+0.1);
			}
			vself = 0;
			voppo = 0;
			hself = 0;
			hoppo = 0;
			d1self = 0;
			d1oppo = 0;
			d2self = 0;
			d2oppo = 0;
		}
		
//		double promising1 = Math.max(vScore, hScore);
//		double promising2 = Math.max(d2Score, d1Score);
//		return Math.max(promising2, promising1);
		return vScore+hScore+d1Score+d2Score;
	}
	
	private static double helper(double self, double oppo) {
		if(oppo > 1) return 0;
		else return Math.pow(100, self);
	}
	
	private static int[][] parser(Piece p, PentagoBoardState pbs) {
		
		int[][] intBoard = new int[PentagoBoardState.BOARD_SIZE][PentagoBoardState.BOARD_SIZE];
    	for(int i = 0; i < intBoard.length; i++) {
			for(int j = 0; j < intBoard[0].length; j++) {
				Piece curP = pbs.getPieceAt(i,j);
				if(curP.equals(p)) intBoard[i][j] = 1;
				else if(curP.equals(Piece.EMPTY)) intBoard[i][j] = 0;
				else intBoard[i][j] = -1;
			}
		}
    	return intBoard;
	}
}
