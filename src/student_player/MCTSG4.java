package student_player;

import boardgame.Board;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoMove;

public class MCTSG4 {
	
	public NodeG4 root;
	public int id;
	public int tiemLimit = 1500;
	long startTime = System.currentTimeMillis();

	public MCTSG4(NodeG4 root) {
		this.id = root.id;
		this.root = root;
	}
	
	public MCTSG4(int id, PentagoBoardState pbs) {
		this.id = id;
		this.root = new NodeG4(id, pbs);
	}
	
	public PentagoMove findMove() {
		int max = 0;
		long elapsed = System.currentTimeMillis() - startTime;
		
        while (elapsed < tiemLimit) {
        	int d =0;
        	NodeG4 promisingNode = root;
            while(promisingNode.children.size()!=0) {
            	d++;
            	//System.out.println("Depth: " + ++d);
            	promisingNode = promisingNode.bestUCTChild();
            }
            if (promisingNode.pbs.getWinner() == Board.NOBODY) {
            	NodeG4 randChild = promisingNode.randomChild();
	            NodeG4.backPropogation(randChild, MyToolsG4.playRandom(id, randChild.pbs));
//	            System.out.println("Root value: " + root.UCTvalue());
            }
            if(d>max) max = d;
            elapsed = System.currentTimeMillis() - startTime;
            
        }
        System.out.println("Max Depth: " + max);
        return root.bestUCTChild().from;
	}
}
