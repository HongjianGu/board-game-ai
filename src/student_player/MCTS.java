package student_player;

import java.util.ArrayList;
import java.util.Collections;

import boardgame.Board;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoMove;

public class MCTS {
	
	public Node root;
	public int id;
	public int tiemLimit = 1800;
	long startTime = System.currentTimeMillis();

	public MCTS(Node root) {
		this.id = root.id;
		this.root = root;
	}
	
	public MCTS(int id, PentagoBoardState pbs) {
		this.id = id;
		this.root = new Node(id, pbs);
	}
	
	public PentagoMove findMove() {
		long elapsed = System.currentTimeMillis() - startTime;
		
        while (elapsed < tiemLimit) {
        	//if((elapsed+1)%600 == 0)System.out.println("Elapsed: "+(elapsed+1));
        	int d =0;
            Node promisingNode = root;
            while(promisingNode.children.size()!=0) {
            	//System.out.println("Depth: " + ++d);
            	promisingNode = promisingNode.bestUCTChild();
            }
            if (promisingNode.pbs.getWinner() == Board.NOBODY) {
            	Node randChild = promisingNode.randomChild();
	            //Node.backPropogation(randChild, MyTools.playRandom(id, randChild.pbs));
//	            System.out.println("Root value: " + root.UCTvalue());
            }
            elapsed = System.currentTimeMillis() - startTime;
            
        }
        return root.bestUCTChild().from;
	}
}
