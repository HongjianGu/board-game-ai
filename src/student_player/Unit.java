package student_player;

public class Unit implements Comparable<Unit> {
	private double score;
	private int index;
	
	//CONSTRUCTOR FOR NODE
	public Unit(double score, int index){
		this.score = score;	
		this.index = index;
	}
	
	//SETTER METHODS FOR NODE
	public void setScore(double score){
		this.score = score;
	}


	//GETTER METHODS for Node
	public double getScore(){
		return score;
	}
	public int getIndex(){
		return index;
	}

	public int compareTo(Unit compareNode) {

		double compareScore = compareNode.getScore();

		//sort in ascending order (i.e. increasing)

		return (int) (this.score - compareScore);
	}

}
