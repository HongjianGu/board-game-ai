package student_player;

import java.util.ArrayList;

import boardgame.Move;

import pentago_swap.PentagoPlayer;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoBoardState.Piece;
import pentago_swap.PentagoMove;

/** A player file submitted by a student. */
public class StudentPlayerG2 extends PentagoPlayer {

    /**
     * You must modify this constructor to return your student number. This is
     * important, because this is what the code that runs the competition uses to
     * associate you with your agent. The constructor should do nothing else.
     */
    public StudentPlayerG2() {
        super("260772647G2");
    }

    /**
     * This is the primary method that you need to implement. The ``boardState``
     * object contains the current state of the game, which your agent must use to
     * make decisions.
     */
    
    private static int id = -1;
    private Piece color;
    
    public Move chooseMove(PentagoBoardState boardState) {
        // You probably will make separate functions in MyTools.
        // For example, maybe you'll need to load some pre-processed best opening
        // strategies...
    	
    	id = boardState.getTurnPlayer();
    	color = id == 0 ? Piece.WHITE : Piece.BLACK;

    	//System.out.println(id);
//    	
//    	Move myMove = boardState.getRandomMove();
//    	
//    	ArrayList<PentagoMove> legalMoves = boardState.getAllLegalMoves();
//    	
//    	for(PentagoMove m : legalMoves) {
//    		if(m.getMoveCoord().getX() == 1 || m.getMoveCoord().getX() == 4) {
//    			if(m.getMoveCoord().getY() == 1 || m.getMoveCoord().getY() == 4) return m;
//    		}
//    	}
//
//    	for(PentagoMove m : legalMoves) {
//    		if(m.getMoveCoord().getX() == 0 || m.getMoveCoord().getX() == 2 || m.getMoveCoord().getX() == 3 || m.getMoveCoord().getX() == 5) {
//    			if(m.getMoveCoord().getY() == 1 || m.getMoveCoord().getY() == 4) return m;
//    		}
//    	}
//    	
//    	for(PentagoMove m : legalMoves) {
//    		if(m.getMoveCoord().getX() == 1 || m.getMoveCoord().getX() == 4) {
//    			if(m.getMoveCoord().getY() == 0 || m.getMoveCoord().getY() == 2 || m.getMoveCoord().getY() == 3 || m.getMoveCoord().getY() == 5) return m;
//    		}
//    	}
//    	


        // Return your move to be processed by the server.
        return MyToolsG2.intelligentMove(color, boardState);
//    	return MyToolsG2.evalMove(color, boardState);
    }
}