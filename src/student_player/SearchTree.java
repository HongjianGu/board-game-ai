package student_player;

import java.util.ArrayList;
import java.util.HashMap;

import boardgame.Board;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoMove;

public class SearchTree {

	/**
	 * A general data structure to maintain a tree of game states for searching
	 */
	
	private HashMap<PentagoBoardState, PentagoMove> moves = new HashMap<PentagoBoardState, PentagoMove>();	// children's corresponding moves
	private ArrayList<PentagoBoardState> children; // children nodes
	private PentagoBoardState root;				   // root node
	private double value;						   // value of current root node
	private boolean leaf;						   // whether it is a leaf
	private int id;								   // the id of student_player
	
	public SearchTree(PentagoBoardState cur, int id) {
		this.root = cur;
		this.id = id;
		initialize();
	}
	
	/**
	 * initialize tree
	 */
	private void initialize () {
    	
    	if(root.getWinner() == Board.NOBODY) {
    		for(PentagoMove m : root.getAllLegalMoves()) {
        		// clone pbs and process the move, then add to children
        		PentagoBoardState child = (PentagoBoardState) root.clone();
        		child.processMove(m);
        		children.add(child);
        		// add corresponding state-move pair to moves
        		moves.put(child, m);
        	}
    	}else {
    		leaf = true;
    		if(root.getWinner() == id) value = 1;
    		else value = 0;
    	}
	}
	
	
}
