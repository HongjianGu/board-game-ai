package student_player;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import boardgame.Board;
import boardgame.Move;
import pentago_swap.PentagoBoardState;
import pentago_swap.PentagoBoardState.Piece;
import pentago_swap.PentagoMove;

public class MyToolsG2 {
	
	public static void main(String[]args) {
//		System.out.println();
//		randomDataGenerator(Piece.BLACK, 10000);
//		randomDataGenerator(Piece.WHITE, 10000);
//		NNrunner nn = new NNrunner();
//		nn.run();
//		nn.learn();
//		nn.record();
	}
	
	//private static HashMap<PentagoBoardState, PentagoMove> moves;	// children's corresponding moves
	
    
    
    public static Move intelligentMove(Piece p, PentagoBoardState pbs){
    	
//    	ArrayList<State> children = getChildState(p, pbs);
//    	Collections.sort(children);
//    	for(State s: children) System.out.println(s.value);
//    	System.out.println("Step value estimated: " + children.get(children.size()-1).value);
//    	return children.get(0).from;
    	double max = -1000;
    	Move bestMove = null;
    	for(PentagoMove m : pbs.getAllLegalMoves()) {
    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
    		child.processMove(m);
    		double val = minimaxEval(2, p, child);
    		if(val>max) {
    			max = val;
    			bestMove = m;
    			//System.out.println("Improving: " + max);
    		}
    	}
    	//System.out.println("Step value estimated: " + max);
    	return bestMove;
    }
    
    public static Move evalMove(Piece p, PentagoBoardState pbs){
    	
    	Move best = pbs.getRandomMove();
    	double max = Double.MIN_VALUE;
    	for(PentagoMove m : pbs.getAllLegalMoves()) {
    		// clone pbs and process the move
    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
    		child.processMove(m);
    		double val = evaluate(p, child);
    		if(val > max) {
    			best = m;
    			max = val;
    			System.out.println("estimated: " + max);
    		}
    	}
    	System.out.println("Step value estimated: " + max);
    	return best;
    }
    
//    public static ArrayList<State> getChildState(Piece p, PentagoBoardState s){
//		ArrayList<State> children = new ArrayList<State>();
//		if(!s.gameOver()) {
//    		for(PentagoMove m : s.getAllLegalMoves()) {
//        		// clone pbs and process the move
//        		PentagoBoardState child = (PentagoBoardState) s.clone();
//        		child.processMove(m);
//        		children.add(new State(p,child,m));
//        	}
//    	}else {
//    		System.out.println("Error: Game Over!");
//    	}
//		return children;
//	}
    
    public static double minimaxEval(int depth, Piece p, PentagoBoardState pbs) {
    	return miniMax(depth, p, pbs, false, -100, 100);
    }
    
    // alpha-beta pruning algorithm seen in class
    public static double miniMax(int depth, Piece p, PentagoBoardState pbs, boolean maximize, double alpha, double beta) {
    	if(depth == 0 || pbs.getWinner() != Board.NOBODY) return SimpleEval.simpleEval(p, pbs);
    	
    	if(maximize) {
//    		if(pbs.getWinner() == id) {alpha = 1;return 1;}
//    		else if(pbs.getWinner() == (1-id)) {alpha = 0; return 0;}
    		for(PentagoBoardState child: getSubStates(pbs)) {
        		alpha = Math.max(alpha, miniMax(depth-1, p, child, !maximize, alpha, beta));
        		if(alpha >= beta) break;
        	}
    		return alpha;
		}else {
//			if(pbs.getWinner() == id) {beta = 1; return 1;}
//    		else if(pbs.getWinner() == (1-id)) {beta = 0; return 0;}
			for(PentagoBoardState child: getSubStates(pbs)) {
				beta = Math.min(beta, miniMax(depth-1, p, child, !maximize, alpha, beta));
        		if(alpha >= beta) break;
        	}
    		return beta;
		}
    }
    
    /**
     * get all children of the current pbs according to all legal moves
     * @param pbs
     * @return children of input pbs
     */
    public static ArrayList<PentagoBoardState> getSubStates(PentagoBoardState pbs) {
    	
    	ArrayList<PentagoBoardState> children = new ArrayList<PentagoBoardState>();
    	Random r = new Random();
    	int count = 60;
    	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
    	Collections.shuffle(moves);
    	if(pbs.getWinner() == Board.NOBODY) {
    		for(PentagoMove m : moves) {
//    			if(r.nextDouble()<0.7) continue;
    			if(count <= 0) break;
        		// clone pbs and process the move, then add to children
        		PentagoBoardState child = (PentagoBoardState) pbs.clone();
        		child.processMove(m);
        		children.add(child);
        		count--;
        	}
    	}
//    	Collections.shuffle(children);
		return children;
    }
    
    public static double evaluate(Piece p, PentagoBoardState pbs) {
    	
    	Piece[][] board = getBoard(pbs);
    	int id = -1;
    	if(p == Piece.WHITE) id = 0;
    	else id = 1;
    	if (id != -1 && pbs.getWinner() == id) return 1;
    	if (id != -1 && pbs.getWinner() != Board.NOBODY && pbs.getWinner() != id) return -1;
		return (horizontal(p, board)*0.8 + vertical(p, board)*0.8 + diagonal(p, board)*0.5)/3 + center(p, board)*0.2;
    }
    
//    public static double NNeval(Piece p, PentagoBoardState pbs) {
//    	return StudentPlayer.nn.eval(boardParser(p, pbs));
//    }
//
//    private static double evalRandom(int id, PentagoBoardState pbs, int rep) {
//    	double win = 0;
//    	int i = 0;
//    	while(i < rep) {
//    		win += playRandom(id, pbs);
//    		i++;
//    	}
//    	return win/rep;
//    }
    
//    public static void randomDataGenerator(Piece p, int rep) {
//    	int[][] trainingData = new int[rep][36];
//    	int[] targets = new int[rep];
//    	int id;
//    	if(p == Piece.WHITE) id = 0;
//    	else id = 1;
//    	
//    	for(int i = 0; i < rep; i++) {
//    		PentagoBoardState pbs = new PentagoBoardState();
//    		targets[i] = playRandom(id, pbs);
//    		trainingData[i] = boardParser(p, pbs);
//    	}
//    	writeResult(trainingData, targets);
//    }
//    
    public static int[] boardParser(Piece p, PentagoBoardState pbs){
    	int[] board = new int[36];
    	for(int j = 0; j < 6; j++) {
			for(int k = 0; k < 6; k++) {
				if(pbs.getPieceAt(k, j) == p) board[j*6+k] = 1;
				else if(pbs.getPieceAt(k, j) == Piece.EMPTY) board[j*6+k] = 0;
				else board[j*6+k] = -1;
			}
		}
    	return board;
    }
    
    private static int playRandom(int id, PentagoBoardState pbs) {
    	
    	if (pbs.getWinner() == id) return 1;
    	else if(pbs.getWinner() != Board.NOBODY) return 0;
    	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
        pbs.processMove(moves.get((int)(Math.random()*moves.size())));
		return playRandom(id,pbs);
    }
    
    private static double horizontal(Piece p, Piece[][] board) {
    	double three = 0;
    	double two   = 0;
    	double one   = 0;
    	
    	for(int k = 0; k < 2; k++) {
    		for(int j = 0; j < 2; j++) {
        		for(int i = j*3; i < 3*j+3; i++) {
    	    		if(board[k][i] == p) {
    	    			if(board[k+1][i] == p) {
    	    				if(board[k+2][i] == p) three++;
    	    				else if(board[k+2][i] == Piece.EMPTY) two++;
    	    				else two += 0.5;
    	    			}else if(board[k+1][i] == Piece.EMPTY) {
    	    				if(board[k+2][i] == p) two += 0.5;
    	    				else if(board[2][i] == Piece.EMPTY) one++;
    	    				else one += 0.5;
    	    			}else {
    	    				if(board[k+2][i] == p) one += 1.5;
    	    				else if(board[k+2][i] == Piece.EMPTY) one += 0.7;
    	    				else one += 0.5;
    	    			}
    	    		}else if(board[k][i] == Piece.EMPTY) {
    	    			if(board[k+1][i] == p) {
    	    				if(board[k+2][i] == p) two++;
    	    				else if(board[k+2][i] == Piece.EMPTY) one++;
    	    				else one += 0.7;
    	    			}else if(board[k+1][i] == Piece.EMPTY) {
    	    				if(board[k+2][i] == p) one ++;
    	//    				else if(board[2][i] == Piece.EMPTY) ;
    	//    				else ;
    	    			}else {
    	    				if(board[k+2][i] == p) one += 0.5;
    	//    				else if(board[2][i] == Piece.EMPTY) ;
    	//    				else ;
    	    			}
    	    		}else {
    	    			if(board[k+1][i] == p) {
    	    				if(board[k+2][i] == p) two += 0.5;
    	    				else if(board[k+2][i] == Piece.EMPTY) one += 0.7;
    	    				else two += 0.5;
    	    			}else if(board[k+1][i] == Piece.EMPTY) {
    	    				if(board[k+2][i] == p) one += 0.7;
    	//    				else if(board[2][i] == Piece.EMPTY) ;
    	//    				else ;
    	    			}else {
    	    				if(board[k+2][i] == p) one += 0.5;
    	//    				else if(board[2][i] == Piece.EMPTY) ;
    	//    				else ;
    	    			}
    	    		}
          		}
        	}
    	}
    	
    	
    	return 0.2*three + 0.05*two + 0.01*one;
    }
    
    private static double vertical(Piece p, Piece[][] board) {
    	Piece[][] transpose = new Piece[6][6];
    	for(int i=0;i<6;i++){    
    		for(int j=0;j<6;j++){    
    			transpose[i][j] = board[j][i];  
    		}
    	}
    	return horizontal(p, transpose);
    }
    
    private static double diagonal(Piece p, Piece[][] board) {
    	int diagR1 = 0;
    	int diagR2 = 0;
    	int diagL1 = 0;
    	int diagL2 = 0;
    	
    	double diagR1chance = 0;
    	double diagR2chance = 0;
    	double diagL1chance = 0;
    	double diagL2chance = 0;
    	
    	for(int k = 0; k < 6; k+=3) {
    		for(int j = 0; j < 6; j+=3) {
    			// diagR1
        		if(board[j+1][k+2] == p && board[j+2][k+1] == p) diagR1++;
        		// diagR2
        		if(board[j][k+1] == p && board[j+1][k] == p) diagR2++;
        		// diagL1
        		if(board[j][k+1] == p && board[j+1][k+2] == p) diagL1++;
        		// diagL2
        		if(board[j+1][k] == p && board[j+2][k+1] == p) diagL2++;
        		// diagR1Chance
        		if(board[j][k] == p) diagR1+=0.25;
        		else if(board[j][k] != Piece.EMPTY) diagR1chance++;
        		// diagR2Chance
        		if(board[j+2][k+2] == p) diagR2+=0.25;
        		else if(board[j+2][k+2] != Piece.EMPTY) diagR2chance++;
        		// diagL1Chance
        		if(board[j+2][k] == p) diagL1+=0.25;
        		else if(board[j+2][k] != Piece.EMPTY) diagL1chance++;
        		// diagL2Chance
        		if(board[j][k+2] == p) diagL2+=0.25;
        		else if(board[j][k+2] != Piece.EMPTY) diagL2chance++;
    		}
    	}
    	
    	diagR1chance /= 4;
    	diagR2chance /= 4;
    	diagL1chance /= 4;
    	diagL2chance /= 4;
    	
    	return (diagR1 * diagR1chance + diagR2 * diagR2chance + diagL1 * diagL1chance + diagL2 * diagL2chance) * 0.25;
    }
    
    private static int center(Piece p, Piece[][] board) {
    	int center = 0;
    	if (board[1][1] == p) center++;
    	if (board[1][4] == p) center++;
    	if (board[4][1] == p) center++;
    	if (board[4][4] == p) center++;
    	
    	return center;
    }
    
//    private static double evalBasic(Piece p, PentagoBoardState pbs) {
//    	return eval(getBoard(p, pbs));
//    }
//    
//    private static double eval(int[][] board) {
//    	double r = 0;
//    	double c = 0;
//    	double d = 0;
//    	double[] rows = new double[6];
//    	double[] cols = new double[6];
//    	double[] diags = new double[6];
//    	
//    	for(int i = 0; i < 6; i++) {
//    		for(int j = 0; j < 6; j++) {
//    			if(j == 1 || j == 4) {
//    				rows[i] += board[j][i]*1.2;
//    				cols[i]	+= board[i][j]*1.2;
//    			}else {
//    				rows[i] += board[j][i];
//    				cols[i] += board[i][j];
//    			}
//    		}
//    		if(i != 5) {
//    			diags[0] += board[i][i+1];
//    			diags[5] += board[i][4-i];
//    		}
//    		if(i != 0) {
//    			diags[2] += board[i][i-1];
//    			diags[3] += board[i][6-i];
//    		}
//    		diags[1] += board[i][i];
//			diags[4] += board[i][5-i];
//    	}
//    	
//    	for(Double rVal: rows) {
//    		r += rVal;
//    	}
//    	for(Double cVal: cols) {
//    		c += cVal;
//    	}
//    	d = diags[1] + diags[4] + (diags[0] + diags[2] + diags[3] + diags[5]) * 0.8;
//    	
//		return r+c+d;
//    }
    
    private static Piece[][] getBoard(PentagoBoardState pbs){
    	
    	Piece[][] board = new Piece[pbs.BOARD_SIZE][pbs.BOARD_SIZE];
    	
    	for(int i = 0; i < pbs.BOARD_SIZE; i++) {
    		for(int j = 0; j < pbs.BOARD_SIZE; j++) {
    			board[i][j] = pbs.getPieceAt(i, j);
    		}
    	}
    	
    	return board;
    }
    
    private static int[][] getBoard(Piece p, PentagoBoardState pbs){
    	
    	int[][] board = new int[pbs.BOARD_SIZE][pbs.BOARD_SIZE];
    	
    	for(int i = 0; i < pbs.BOARD_SIZE; i++) {
    		for(int j = 0; j < pbs.BOARD_SIZE; j++) {
    			if(pbs.getPieceAt(i, j) == p) board[i][j] = 1;
    			else if(pbs.getPieceAt(i, j) == Piece.EMPTY) board[i][j] = 0;
    			else board[i][j] = -2;
    		}
    	}
    	
    	return board;
    }
	
    private static void writeResult(int[][] trainingData, int[] targets) {
		try {
			FileWriter fw = new FileWriter("data/train.txt");

		    for (int i = 0; i < trainingData.length; i++) {
		    	for(int j = 0; j < trainingData[i].length; j++) {
		    		fw.write(""+trainingData[i][j]);
		    		if(j != trainingData[i].length - 1) fw.write(";");
		    	}
		    	fw.write("\n");
		    }
		    
		    fw.close();
		    
		    FileWriter fw2 = new FileWriter("data/target.txt");
		    
		    for (int i = 0; i < targets.length; i++) {
	    		fw2.write(""+targets[i]);
		    	fw2.write("\n");
		    }
		    
		    fw2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}  
    }
}