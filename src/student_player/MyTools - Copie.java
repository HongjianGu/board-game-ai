//package student_player;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Random;
//
//import boardgame.Board;
//import boardgame.Move;
//import pentago_swap.PentagoBoardState;
//import pentago_swap.PentagoBoardState.Piece;
//import pentago_swap.PentagoMove;
//
//public class MyTools {
//	
//	private static final double MIN = -1000000000;
//	private static final double MAX = 1000000000;
//	
//	public static Move minimaxMove(Piece p, PentagoBoardState pbs){
//    	double max = -1000000000;
//    	Move bestMove = null;
//    	for(PentagoMove m : pbs.getAllLegalMoves()) {
//    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
//    		child.processMove(m);
//    		double val = miniMax(2, p, child, false, -1000000000, 1000000000);
//    		if(val>max) {
//    			max = val;
//    			bestMove = m;
//    			//System.out.println("Improving: " + max);
//    		}
//    	}
//    	//System.out.println("Step value estimated: " + max);
//    	return bestMove;
//    }
//    
//    // alpha-beta pruning algorithm seen in class
//    public static double miniMax(int depth, Piece p, PentagoBoardState pbs, boolean maximize, double alpha, double beta) {
//    	if(depth == 0 || pbs.getWinner() != Board.NOBODY) return EvalFun.evalBoard(p, pbs);
//    	
//    	if(maximize) {
//    		for(PentagoBoardState child: getSubStates(pbs)) {
//        		alpha = Math.max(alpha, miniMax(depth-1, p, child, !maximize, alpha, beta));
//        		if(alpha >= beta) break;
//        	}
//    		return alpha;
//		}else {
//			for(PentagoBoardState child: getSubStates(pbs)) {
//				beta = Math.min(beta, miniMax(depth-1, p, child, !maximize, alpha, beta));
//        		if(alpha >= beta) break;
//        	}
//    		return beta;
//		}
//    }
//    
//    /**
//     * get all children of the current pbs according to all legal moves
//     * @param pbs
//     * @return children of input pbs
//     */
//    public static ArrayList<PentagoBoardState> getSubStates(PentagoBoardState pbs) {
//    	
//    	ArrayList<PentagoBoardState> children = new ArrayList<PentagoBoardState>();
//    	Random r = new Random();
//    	int count = 55;
//    	ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
//    	Collections.shuffle(moves);
//    	if(pbs.getWinner() == Board.NOBODY) {
//    		for(PentagoMove m : moves) {
////    			if(r.nextDouble()<0.7) continue;
//    			if(count <= 0) break;
//        		// clone pbs and process the move, then add to children
//        		PentagoBoardState child = (PentagoBoardState) pbs.clone();
//        		child.processMove(m);
//        		children.add(child);
//        		count--;
//        	}
//    	}
//    	Collections.shuffle(children);
//		return children;
//    }
//	
//    public static Move intelligentMove(Piece p, PentagoBoardState pbs){
//    	
//    	double max = MIN;
//    	Move bestMove = null;
//    	for(PentagoMove m : getMinSubMoves(p, pbs)) {
//    		PentagoBoardState child = (PentagoBoardState)pbs.clone();
//        	double val = miniMax(3, p, m, false, MIN, MAX, child);
//    		if(val>max) {
//    			max = val;
//    			bestMove = m;
////    			System.out.println("Improving: " + max);
//    		}
//    	}
////    	System.out.println("Step value estimated: " + max);
//    	return bestMove;
//    }
//    
//    // alpha-beta pruning algorithm seen in class
//    public static double miniMax(int depth, Piece p, PentagoMove move, boolean maximize, double alpha, double beta, PentagoBoardState origin) {
//    	if(depth == 0||origin.getWinner() != Board.NOBODY) return Heuristic.defenseEval(p, move, origin);
//    	origin = (PentagoBoardState) origin.clone();
//    	origin.processMove(move);
//    	
////    	if(origin.getWinner() != Board.NOBODY) return Heuristic.evalBoard(p, origin);
////    	if(depth == 0) {
////    		origin = (PentagoBoardState) origin.clone();
////    		origin.processMove(move);
////    		return Heuristic.evalBoard(p, origin);
////    	}
//    	
////    	if(origin.getWinner() != Board.NOBODY) return Heuristic.evalBoard(p, origin);
//    	if(maximize) {
////    		ArrayList<PentagoMove> moves = origin.getAllLegalMoves();
//    		ArrayList<PentagoMove> moves = getMinSubMoves(p, origin);
//    		for(PentagoMove m: moves) {
//    			alpha = Math.max(alpha, miniMax(depth-1, p, m, !maximize, alpha, beta, origin));
//        		if(alpha >= beta) return alpha;
//    		}
//			return alpha;
//		}else {
////    		ArrayList<PentagoMove> moves = origin.getAllLegalMoves();
//			ArrayList<PentagoMove> moves = getMinSubMoves(p, origin);
//			for(PentagoMove m: moves) {
//				beta = Math.min(beta, miniMax(depth-1, p, m, !maximize, alpha, beta, origin));
//	    		if(alpha >= beta) return beta;
//        	}
//    		return beta;		
//		}
//    }
//    
//
//    public static ArrayList<PentagoMove> getMaxSubMoves(Piece p, PentagoBoardState pbs) {
//    	
//    	ArrayList<PentagoMove> children = new ArrayList<PentagoMove>();
//		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
//    	Collections.shuffle(moves);
//		double max = MIN;
//		for(PentagoMove m :moves) {
//    		// clone pbs, then add to children
//    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
////    		Piece p = child.getTurnPlayer() == 0 ? Piece.WHITE : Piece.BLACK;
//    		double val = Heuristic.defenseEval(p, m, child);
//    		if(val>max) {
//    			max = val;
//    			children.add(m);
//    		}
//    	}
//    	Collections.shuffle(children);
//		return children;
//    }
//    
//    public static ArrayList<PentagoMove> getMinSubMoves(Piece p, PentagoBoardState pbs) {
//    	
//    	ArrayList<PentagoMove> children = new ArrayList<PentagoMove>();
//		ArrayList<PentagoMove> moves = pbs.getAllLegalMoves();
//    	Collections.shuffle(moves);
////		double min = MAX;
////		for(PentagoMove m :moves) {
////    		// clone pbs, then add to children
////    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
//////    		Piece p = child.getTurnPlayer() == 1 ? Piece.WHITE : Piece.BLACK;
////    		double val = Heuristic.defenseEval(p, m, child);
////    		if(val<min) {
////    			min = val;
////    			children.add(m);
////    		}
////    	}
//    	double max = MIN;
//		for(PentagoMove m :moves) {
//    		// clone pbs, then add to children
//    		PentagoBoardState child = (PentagoBoardState) pbs.clone();
//    		p = child.getTurnPlayer() == 0 ? Piece.WHITE : Piece.BLACK;
//    		double val = Heuristic.defenseEval(p, m, child);
//    		if(val>max) {
//    			max = val;
//    			children.add(m);
//    		}
//    	}
//    	Collections.shuffle(children);
//		return children;
//    }
//}