package student_player;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

import pentago_swap.PentagoBoardState;

// Neural network class to compute the weights for evaluation function
public class NN {
    public int[][] 		allInputs;		// training data
    public int 			inputSize;
    public int[] 		targets;		// training targets
    public double[] 	hiddenBias;		// randomly assigned bias value for each hidden layer neuron
    public double[] 	finalWeights;	// weights of each hidden layer neuron's output into the final output neuron
    public double[][] 	inputWeights;	// weights of each input layer neurons's output into each hidden layer neuron
    public double 		finalOut;		// the output of the final output neuron in the output layer
    public int 			numHidden;		// the number of hidden layer neurons
    public double 		finalBias;		// bias when giving final output
    public double[] 	hiddenOutputs;	// the outputs from each hidden layer neuron
    public Modifier 	modify;			// the modifier which modifies all the weights and bias for each iteration
    public int 			rep = 5000; 	// number of times of training

//    public static void main(String[] args) {
//    	NN restore = new NN();
//    	restore.finalBias = randDouble(-0.5f, 0.5f);
//        for (int i = 0;i<restore.numHidden;i++){
//        	restore.hiddenBias[i] 	 = randDouble(-0.5f, 0.5f);
//        	restore.finalWeights[i] = randDouble(-0.5f, 0.5f);
//            for (int j = 0;j<restore.inputSize;j++) {
//            	restore.inputWeights[j][i] = randDouble(-0.5f, 0.5f);
//            }
//        }
//        restore.recordPara();
//    }
    
    public NN() {
    	//this.allInputs = new int[1][36];
        //this.targets   = targets;
    	this.inputSize	   = 36;
        this.numHidden 	   = 18;
        this.hiddenOutputs = new double[numHidden];
        
        this.hiddenBias   = new double[numHidden];
        this.finalWeights = new double[numHidden];
        this.inputWeights = new double[36][numHidden];
        try {
			loadConfig();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public NN (int[][] inputs, int[] targets) {
        this.allInputs = inputs;
        this.targets   = targets;
        
        this.inputSize = allInputs[0].length;

        this.numHidden 	   = inputSize/2;
        this.hiddenOutputs = new double[numHidden];
        
        this.hiddenBias   = new double[numHidden];
        this.finalWeights = new double[numHidden];
        this.inputWeights = new double[inputSize][numHidden];
    }
    
    public void recordPara () {
    	//System.out.println("Recording parameters...");
    	writeResult(finalBias, hiddenBias, finalWeights, inputWeights);
    	//System.out.println("Recording completed");
    }

    // the function which does most of the work of learning
    public void learn() {
        //float quadraticErr = 0;
        // repeat for training
    	// randomly initialize all bias and all weights
        finalBias = randDouble(-0.5f, 0.5f);
        for (int i = 0;i<numHidden;i++){
            this.hiddenBias[i] 	 = randDouble(-0.5f, 0.5f);
            this.finalWeights[i] = randDouble(-0.5f, 0.5f);
            for (int j = 0;j<inputSize;j++) {
                this.inputWeights[j][i] = randDouble(-0.5f, 0.5f);
            }
        }
        
    	System.out.println("Learning...");
        for (int i = 0; i<rep; i++) {
        	if((i+1)%100 == 0) System.out.println("Completed " + (i+1) +" iterations");
            for (int j = 0; j<targets.length; j++) {
                finalOut = eval(getInputs(j)); // calculate the final output for the current training sample
                modify = new Modifier (targets[j], finalOut, hiddenOutputs, finalWeights, inputWeights, 
                		hiddenBias, finalBias, numHidden, getInputs(j), inputSize);
                // update all the weights and bias
                update();
                
                //quadraticErr += Math.pow(((targets[j] - finalOut)), 2);
            }
            //quadraticErr /= 2;
        }
        System.out.println("Learning finished");   

//            float[] valueToPredict = new float[] {-0.205f, 0.780f};
//            System.out.println("Predicted result: " + (((calOutput(valueToPredict)) < 0.5) ? 0 : 1));
    }
    
    public void update() {
    	finalWeights = modify.finalWeights;
        inputWeights = modify.inputWeights;
        hiddenBias   = modify.hiddenBias;
        finalBias    = modify.finalBias;
    }
    
    public double learningEval(int[] inputs, double randomResult) {
//    	try {
//			loadConfig();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
    	finalOut = eval(inputs);
    	modify = new Modifier (randomResult, finalOut, hiddenOutputs, finalWeights, inputWeights, 
        		hiddenBias, finalBias, numHidden, inputs, inputSize);
    	update();
//    	recordPara();
    	return finalOut;
    }
    
    
    // get the outputs of the input layer neurons in the nth sample
    private int[] getInputs(int n){
    	int[] inLayerOut = new int[inputSize];
        for (int i = 0; i<inputSize; i++){
        	inLayerOut[i] = this.allInputs[n][i];
        }
        return inLayerOut;
    }
    
    public double eval(int[] inputVal){
    	// for each hidden layer neuron, calculate its corresponding inputs and outputs
        for (int i = 0;i<numHidden;i++){
        	double sum = 0; // sum of weighted inputs into a hidden layer neuron
            for (int j=0; j<inputSize; j++){
            	// loop through all input neurons and sum up their weighted outputs to the hidden neuron
                sum = sum + (inputVal[j] * inputWeights[j][i]);
            }                                 
            // use sigmoid function to calculate the outputs of each hidden layer neuron
            hiddenOutputs[i] = sigmoid(sum + hiddenBias[i]);
        }

        finalOut = 0; // clear output value
        // sum up all weighted inputs from the hidden layer neurons to get the final output
        for (int i = 0;i<numHidden;i++){
        	finalOut += hiddenOutputs[i] * finalWeights[i];
        }
        return sigmoid(finalOut + finalBias);
    }
    
    // sigmoid function for output calculation
    private double sigmoid(double value) {
    	return (double)(1/(1+Math.exp(-value)));
    }
    
    // get a random float number between min and max
    private static double randDouble(double min, double max) {
        Random rand = new Random();
        return min + rand.nextDouble() * (max - min);
    }
    
    private static void writeResult(double finalBias, double[] hiddenBias, double[] finalWeights, double[][] inputWeights) {
		try {
			FileWriter fw = new FileWriter("data/configuration.txt");
			fw.write(""+finalBias+"\n");
			for(int i = 0; i < hiddenBias.length; i ++) {
				fw.write(""+hiddenBias[i]);
				if(i != hiddenBias.length-1) fw.write(";");
			}
			fw.write("\n");
			
			for(int i = 0; i < finalWeights.length; i ++) {
				fw.write(""+finalWeights[i]);
				if(i != finalWeights.length-1) fw.write(";");
			}
			fw.write("\n");
			
			for(int i = 0; i < inputWeights.length; i ++) {
				for(int j = 0; j < inputWeights[i].length; j ++) {
					fw.write(""+inputWeights[i][j]);
					if(i == inputWeights.length-1 && j == inputWeights[i].length-1) break;
					fw.write(";");
				}
			}
		    fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}  
    }
    
    public void loadConfig() throws IOException{
    	//System.out.println("Loading configuration...");
        List<String> lines = Files.readAllLines(Paths.get("data/configuration.txt"));
        finalBias = Double.parseDouble(lines.get(0));
        String[] hidB = lines.get(1).split(";");
        for(int i = 0; i < hidB.length; i++){
        	hiddenBias[i] = Double.parseDouble(hidB[i]);
        }
        
        String[] finWeights = lines.get(2).split(";");
        for(int i = 0; i < finWeights.length; i++){
        	finalWeights[i] = Double.parseDouble(finWeights[i]);
        }
        
        String[] inWeights = lines.get(3).split(";");
        for(int i = 0; i < inWeights.length; i++){
        	int j = i%18;
        	int k = i/36;
        	inputWeights[k][j] = Double.parseDouble(inWeights[i]);
        }
        //System.out.println("Configuration Completed");
    }
}
