package student_player;

public class Modifier {
    public double[]	 	hiddenBias;
    public double[]	 	finalWeights;
    public double[][] 	inputWeights;
    public double 	 	finalBias;

    public Modifier(double target, double finalOut, double[] hiddenOutputs, double[] finalWeights, double[][] inputWeights, double[] hiddenBias, double finalBias, int numHidden, int[] inputVal, int numInNeuron){
        this.hiddenBias 		= new double[numHidden];				// updated bias of hidden layer neurons
        this.finalWeights 		= new double[numHidden];				// updated weights of output from hidden layer neurons
        this.inputWeights 		= new double[numInNeuron][numHidden];	// updated weights of outputs from input layer neurons
        double[]   deltaHid		= new double[numHidden];				// delta of hidden layer neurons
        double[][] inputWChange	= new double[numInNeuron][numHidden];	// the change made to every input weights
        double[]   deltaHidBias	= new double[numHidden];				// derivative of bias of hidden layer neurons
        double[]   hidBiasChange	= new double[numHidden];			// the change made to the bias of every hidden layer neurons
        double  error  			= target - finalOut;					// difference between the target and the actual final output
        double	lRate 			= 0.05;									// learning rate
        if(Math.abs(error)>0.2)System.out.println("Error: " + error);
        // Modify the weights of outputs from the hidden layer neurons into the final output neuron
        double derivFinalOut = finalOut * (1-finalOut) * error;			// derivative of v
        for (int i = 0;i<numHidden;i++){
            this.finalWeights[i] = finalWeights[i] + lRate * derivFinalOut * hiddenOutputs[i];
        }

        // Modify final output bias
        this.finalBias = finalBias + lRate * derivFinalOut;

        // Modify weights from the input layer neurons
        for (int i = 0;i<numHidden;i++){
        	deltaHid[i] = hiddenOutputs[i] * (1 - hiddenOutputs[i]) * finalWeights[i] * derivFinalOut;
            for (int j = 0;j<numInNeuron;j++){ // for the jth input neuron
            	inputWChange[j][i] = lRate * deltaHid[i] * inputVal[j]; // the value of change of the weight of the jth input neuron
                this.inputWeights[j][i] = inputWeights[j][i] + inputWChange[j][i];
            }
        }

        // Modify bias of the hidden layer neurons
        for (int i = 0;i<numHidden;i++){
        	deltaHidBias[i] = hiddenOutputs[i] * (1 - hiddenOutputs[i]) * finalWeights[i] * derivFinalOut;
        	hidBiasChange[i] = lRate * deltaHidBias[i];
            this.hiddenBias[i] = hiddenBias[i] + hidBiasChange[i];
        }
        
        // Modify the weights of outputs from the hidden layer neurons into the final output neuron
//        double delta = finalOut * (1-finalOut) * error;			// = - (target - finalOut)(finalOut * (1-finalOut))
//        for (int i = 0;i<numHidden;i++){
//            this.finalWeights[i] = finalWeights[i] + lRate * hiddenOutputs[i] * delta;
//        }
//
//        // Modify final output bias
//        this.finalBias = finalBias + lRate * delta;
//
//        // Modify weights from the input layer neurons
//        for (int i = 0;i<numHidden;i++){
//        	deltaHid[i] = hiddenOutputs[i] * (1 - hiddenOutputs[i]) * finalWeights[i] * delta;
//            for (int j = 0;j<numInNeuron;j++){ // for the jth input neuron
//                this.inputWeights[j][i] = inputWeights[j][i] + lRate * deltaHid[i] * inputVal[j];
//            }
//        }
//
//        // Modify bias of the hidden layer neurons
//        for (int i = 0;i<numHidden;i++){
//            this.hiddenBias[i] = hiddenBias[i] + lRate * deltaHid[i];
//        }
    }
}